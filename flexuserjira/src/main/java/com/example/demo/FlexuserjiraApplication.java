package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class FlexuserjiraApplication {
	
	 @Path("/user/login/test")
    public String userLoginTest(@QueryParam("username") String userName) {
        LicenseEntity licenseEntity = ao.getLicense();

        ApplicationUser user = ComponentAccessor.getUserManager().getUserByName(userName);

        //check group
        boolean isTarget = false;
        String targetGroup = licenseEntity.getTargetGroup(); //canuse

        com.atlassian.crowd.embedded.api.User cUser = crowdService.getUser(userName);
        com.atlassian.crowd.embedded.api.Group tg = crowdService.getGroup(targetGroup);

        isTarget = crowdService.isUserMemberOfGroup(cUser, tg);

        try {
            if (!isTarget) {
                log.warn("\n" + cUser.getName() + ".. login processing.. target: {}, source: {}\n", targetGroup, licenseEntity.getSourceGroup());
                ComponentAccessor.getGroupManager().addUserToGroup(user, tg);
                return "remove user from canuse group";
            }
        } catch (Exception e) {
            return e.getMessage();
        }
        try{
           crowdService.removeUserFromGroup(cUser, tg);
        }catch (Exception e){
            return e.getMessage();
        }
        return "add user to canuse group";
    }

	 
	public static void main(String[] args) {
		SpringApplication.run(FlexuserjiraApplication.class, args);
	}

}
